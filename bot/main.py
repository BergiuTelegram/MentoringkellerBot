# Dependencies:
#   pip3 install bs4
#   pip3 install python-telegram-bot

import logging
import sys
import telegram
from telegram.ext import Updater, CommandHandler
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from typing import List


LICENSE_TEXT = 'Welcome!\nThis bot is a program which is available under the MIT license at https://gitlab.com/BergiuTelegram/MentoringkellerBot'

DSGVO_TEXT = "Dieser Bot speichert <b>keine persönlichen Daten</b>."

HELP_TEXT = "Dieser Bot kann die ungefähre Anzahl an Personen im Mentoringkeller anzeigen.\n\nBefehle:\n/help - <i>Zeigt diese Hilfenachricht an</i>\n/license - <i>Zeigt die Lizenz an</i>\n/dsgvo - <i>Unsere Datenschutzerklärung</i>\n/count - <i>Anzahl der Personen im Mentoringkeller</i>\nDie Zahlen sind die Anzahl der mit den WLAN APs verbundenen Geräte mit dem 2.4GHz WLAN (linke Zahl) und 5GHz WLAN (rechte Zahl) geteilt durch 2 um eine ungefähre Abschätzung der Anzahl an Personen zu liefern. Die Frequenzen werden getrennt angezeigt, weil das 2.4GHz Netzwerk von viel weiter weg noch erreicht werden kann und dadurch ungenauer ist."


def license(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=LICENSE_TEXT, parse_mode=telegram.ParseMode.HTML)


def dsgvo(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=DSGVO_TEXT, parse_mode=telegram.ParseMode.HTML)


def myhelp(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=HELP_TEXT, parse_mode=telegram.ParseMode.HTML)


class AP:
    """
    AccessPoint name and its alias
    """

    def __init__(self, name, alias):
        self.name = name
        self.alias = alias


class APCount:
    """
    An AP and its connected users counts
    """

    def __init__(self, ap: AP, two_ghz_count, five_ghz_count):
        self.ap = ap
        self.counts = {
                "2.4ghz": two_ghz_count,
                "5ghz": five_ghz_count,
                }


APS = [AP("ap-2356-6u07", "OG Mentoringkeller"),
       AP("ap-2356-6u09", "Mentoring²Keller²")]
URL = "https://noc-portal.rz.rwth-aachen.de/mops-admin/coverage?iib=173"


def floor_count(x: int) -> str:
    if x == 1:
        return str(x)
    return str(int(x/2))


def get_counts(children):
    two_ghz_count = 0
    five_ghz_count = 0
    i = 0
    for child in children:
        if i == 1:
            # name
            name = child.contents[0]
        if i == 3:
            # 2.4ghz
            two_ghz_count += int(child.contents[0])
        if i == 5:
            # 5ghz
            five_ghz_count += int(child.contents[0])
        i += 1
    return (two_ghz_count, five_ghz_count)



def get_apcounts(url, aps: List[AP]) -> List[APCount]:
    soup = bs(urlopen(url), features="html.parser")
    out = []
    # foreach row
    for html_row in soup.findAll("tr"):
        name_ = html_row.td
        if name_ is None:
            continue
        name = name_.string
        # if ap is requested
        ap = None
        for ap_ in aps:
            if name == ap_.name:
                ap = ap_
        if ap is None:
            continue
        # create ap count
        counts = get_counts(html_row.children)
        two_ghz_count = counts[0]
        five_ghz_count = counts[1]
        apcount = APCount(ap, two_ghz_count, five_ghz_count)
        # add ap count to output
        out.append(apcount)
    return out


def count(bot, update):
    apcounts = get_apcounts(URL, APS)
    name = ""
    text = "Mentoringkeller \nungefähre Personenanzahl:\n\n"
    text += "<code>"
    for apcount in apcounts:
        text += apcount.ap.alias + ": "
        text += floor_count(apcount.counts["2.4ghz"]) + " | "
        text += floor_count(apcount.counts["5ghz"]) + "\n"
    text += "</code>"
    bot.send_message(chat_id=update.message.chat_id, text=text, parse_mode=telegram.ParseMode.HTML)


def main():
    if len(sys.argv) < 1:
        print("ERROR: first parameter must be the telegram token")
        sys.exit(1)
    token = sys.argv[1]
    updater = Updater(token=token)
    dispatcher = updater.dispatcher
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    count_handler = CommandHandler('count', count)
    dispatcher.add_handler(count_handler)
    license_handler = CommandHandler('license', license)
    dispatcher.add_handler(license_handler)
    dsgvo_handler = CommandHandler('dsgvo', dsgvo)
    dispatcher.add_handler(dsgvo_handler)
    help_handler = CommandHandler('help', myhelp)
    dispatcher.add_handler(help_handler)
    updater.start_polling()


main()
