#!/bin/bash
version=`cat VERSION`
docker build -t bergiu/mentoringkellerbot:$version .
docker build -t bergiu/mentoringkellerbot:latest .
docker push bergiu/mentoringkellerbot
